from django.urls import path
from . import views

app_name = "story9App"

urlpatterns = [
    path('', views.story9),
    path('auth/', views.auth, name="authentication_page"),
    path('logout/', views.logout_user, name="logout"),
    path('api/v1/login/', views.api_login, name="login_endpoint"),
    path('api/v1/signup/', views.api_sign_up, name="sign_up_endpoint"),
]
