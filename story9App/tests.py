from django.test import TestCase
from django.test import Client
from django.apps import apps
from .apps import Story9AppConfig
from django.contrib.auth.models import User
from django.contrib import auth
from . import views
import json


class UnitTestForStory9(TestCase):
    def test_apps(self):
        self.assertEqual(Story9AppConfig.name, 'story9App')
        self.assertEqual(apps.get_app_config('story9App').name, 'story9App')
    
    def test_url_landing_page(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_template_landing_page(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, "index.html")
    
    def test_setup(self):
        self.client = Client()
        self.response = self.client.get('/story9/')
        self.page_content = self.response.content.decode('utf8')
    
    def test_login(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 302)

    def test_api_bad_request(self):
        response = Client().get('/story9/api/v1/login/')
        self.assertEqual(response.status_code, 400)

        response = Client().get('/story9/api/v1/signup/')
        self.assertEqual(response.status_code, 400)
    
    def test_failed_when_login(self):
        input_user = {'username' : 'Daryl', 'password' : '021201'}
        response = Client().post('/story9/api/v1/login/', json.dumps(input_user), content_type="application/json")
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'status': 401,
            'message': 'Username does not exist'}
        )
    
    def test_when_signup(self):
        userInput = {'username' : 'Cip', 'email' : 'defirsta@gmail.com', 'password' : '121201'}
        response = Client().post('/story9/api/v1/signup/', json.dumps(userInput), content_type="application/json")
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'status': 200,
            'message': 'Your account is now ready to use'}
        )
    
    def test_logout_user(self):
        response = Client().get('/story9/logout/')
        self.assertEqual(response.status_code, 302)

    def test_auth_page(self):
        response = Client().get('/story9/auth/')
        self.assertTemplateUsed(response, 'story9/login.html')
 