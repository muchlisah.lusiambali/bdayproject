"""
bdaycipe-project URL Configuration
"""

from django.contrib import admin
from django.urls import path
from django.urls import include 
from .views import index
#from . import views

admin.autodiscover()

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',index),
    path('story9/', include("story9App.urls"))
]